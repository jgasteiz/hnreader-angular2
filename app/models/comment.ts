export class Comment {
    id: number;
    author: string;
    body: string;
    replies: Comment[];

    constructor(id: number, author:string, body: string, replies: Comment[]) {
        this.id = id;
        this.author = author;
        this.body = body;
        this.replies = replies;
    }
}
