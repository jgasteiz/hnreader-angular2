export class Article {
    id: number;
    title: string;
    link: string;
    votes: number;
    author: string;
    numComments: number;

    readableUrl: string;
    commentsRoute: string;

    constructor(id: number, title: string, link: string, votes?: number, author?: string, numComments?: number) {
        this.id = id;
        this.title = title;
        this.link = link;
        this.votes = votes || 0;
        this.author = author || '';
        this.numComments = numComments || 0;

        if (this.link.indexOf('http') > -1) {
            this.readableUrl = this.link
                .replace('http://', '')
                .replace('https://', '')
                .split('/')[0];
        } else {
            this.link = 'https://news.ycombinator.com/' + this.link;
            this.readableUrl = 'news.ycombinator.com';
        }

        this.commentsRoute = '/comments/' + this.id;
    }
}
