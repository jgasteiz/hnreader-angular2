import { Article } from '../models/article';
import { Comment } from '../models/comment';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {
    // List of articles.
    articles: Article[];

    constructor (private http: Http) {
        this.articles = [];
    }

    /**
     * Fetch the articles from the api.
     * @return {[type]} [description]
     */
    fetchArticlesFromAPI () {
        this.articles = [];
        this.http.get('https://node-hnapi-javiman.herokuapp.com/news')
            .map(response => response.json())
            .subscribe(articles => {
                articles.forEach(function (article: any) {
                    this.articles.push(new Article(
                        article.id,
                        article.title,
                        article.url,
                        article.points,
                        article.user,
                        article.comments_count
                    ));
                }, this);
            });
    }

    /**
     * Fetch the comments from the hn api.
     * @param  {number} itemId [description]
     * @return {[type]}        [description]
     */
    fetchCommentsFromAPI (itemId: number) {
        return this.http.get('https://node-hnapi-javiman.herokuapp.com/item/' + itemId)
            .map(response => response.json());
    }

    /**
     * Given an article or a comment, return its comments if it has any.
     * @param  {any}       item [description]
     * @return {Comment[]}      [description]
     */
    getCommentsFromItem (item: any) :Comment[] {
        var comments: Comment[] = [];
        if (item.comments && item.comments.length > 0) {
            item.comments.forEach(function (comment: any) {
                comments.push(new Comment(
                    comment.id,
                    comment.user,
                    comment.content,
                    this.getCommentsFromItem(comment)
                ));
            }, this);
        }
        return comments;
    }
}
