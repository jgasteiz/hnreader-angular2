import { provideRouter, RouterConfig } from '@angular/router';
import { ArticleListComponent } from './article-list.component';
import { CommentsComponent } from './comments.component';

export const routes: RouterConfig = [
    { path: '', component: ArticleListComponent },
    { path: 'comments/:id', component: CommentsComponent }
];

export const APP_ROUTER_PROVIDERS = [provideRouter(routes)];
