import { Component } from '@angular/core';
import { HTTP_PROVIDERS } from '@angular/http';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { DataService } from '../services/data.service';

@Component({
    directives: [ROUTER_DIRECTIVES],
    providers: [HTTP_PROVIDERS, DataService],
    selector: 'app',
    styleUrls: ['app/styles/app.component.css'],
    templateUrl: 'app/templates/app.component.html'
})
export class AppComponent {
    constructor (private dataService: DataService) {}

    ngOnInit () {
        this.dataService.fetchArticlesFromAPI();
    }
}
