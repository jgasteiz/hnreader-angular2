import { Component } from '@angular/core';
import { HTTP_PROVIDERS } from '@angular/http';
import { ArticleComponent } from './article.component';
import { Article } from '../models/article';
import { DataService } from '../services/data.service';

@Component({
    directives: [ArticleComponent],
    templateUrl: 'app/templates/article-list.component.html',
    styleUrls: ['app/styles/article-list.component.css']
})
export class ArticleListComponent {
    constructor (private dataService: DataService) {}

    /**
     * Return the data service article list.
     * @return {Article[]} [description]
     */
    getArticles (): Article[] {
        return this.dataService.articles;
    }
}
