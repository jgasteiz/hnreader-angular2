import { Component } from '@angular/core';
import { Comment } from '../models/comment';
import { CommentComponent } from './comment.component';

@Component({
    directives: [CommentComponent, CommentListComponent],
    inputs: ['comments'],
    selector: 'comment-list',
    templateUrl: 'app/templates/comment-list.component.html',
    styleUrls: ['app/styles/comment-list.component.css']
})
export class CommentListComponent {
    comments: Comment[]
}
