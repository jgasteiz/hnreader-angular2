import { Component } from '@angular/core';
import { HTTP_PROVIDERS } from '@angular/http';
import { ActivatedRoute, Router, ROUTER_DIRECTIVES } from '@angular/router';
import { DataService } from '../services/data.service';
import { Article } from '../models/article';
import { Comment } from '../models/comment';
import { CommentListComponent } from './comment-list.component';

@Component({
    directives: [CommentListComponent, ROUTER_DIRECTIVES],
    templateUrl: 'app/templates/comments.component.html',
    styleUrls: ['app/styles/comments.component.css']
})
export class CommentsComponent {
    comments: Comment[];
    articleTitle: string;
    articleUrl: string;

    constructor (
        private route: ActivatedRoute,
        private router: Router,
        private dataService: DataService
    ) {}

    ngOnInit () {
        this.articleTitle = '';
        this.articleUrl = '';
        this.comments = [];

        this.route.params.subscribe((params: any) => {
            let id = +params['id']; // (+) converts string 'id' to a number
            this.dataService.fetchCommentsFromAPI(id)
                .subscribe((article: any) => {
                    this.articleTitle = article.title;
                    this.articleUrl = article.url;

                    if (article.comments && article.comments.length > 0) {
                        article.comments.forEach(function (comment: any) {
                            this.comments = this.dataService.getCommentsFromItem(article);
                        }, this);
                    }
                });
        });

    }
}
