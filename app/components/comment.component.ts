import { Component } from '@angular/core';
import { Comment } from '../models/comment';

@Component({
    inputs: ['comment'],
    selector: 'comment',
    templateUrl: 'app/templates/comment.component.html',
    styleUrls: ['app/styles/comment.component.css']
})
export class CommentComponent {
    comment: Comment;
}
