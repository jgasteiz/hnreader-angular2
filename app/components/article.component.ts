import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { Article } from '../models/article';

@Component({
    directives: [ROUTER_DIRECTIVES],
    inputs: ['article', 'index'],
    selector: 'article',
    templateUrl: 'app/templates/article.component.html',
    styleUrls: ['app/styles/article.component.css']
})
export class ArticleComponent {
    article: Article;
    index: number;
}
